﻿using System;
using System.Reflection;

namespace ATMLinq
{
	public class User
	{
		public int Id { get; set; }

		public string FirstName { get; set; }

		public string LastName { get; set; }

		public string MiddleName { get; set; }

		public string PhoneNumber { get; set; }

		public string PassportData { get; set; }

		public DateTime RegDate { get; set; }

		public string Login { get; set; }

		public string Password { get; set; }

		public override string ToString()
		{
			var result = "";
			PropertyInfo[] props = GetType().GetProperties();

			foreach (var p in props)
			{
				result += $"{p.Name}: {p.GetValue(this)}\n";
			}
			return result;
		}
	}
}
