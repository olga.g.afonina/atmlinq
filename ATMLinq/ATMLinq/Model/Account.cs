﻿using System;
using System.Reflection;

namespace ATMLinq
{
	public class Account
	{
		public int Id { get; set; }

		public DateTime StartDate { get; set; }

		public decimal Amount { get; set; }

		public int IdOwner { get; set; }

		public override string ToString()
		{
			return $"Id = {Id}\t IdOwner = {IdOwner}\t StartDate = {StartDate.ToShortDateString()}\t Amount = {Amount:C0} ";
		}
	}
}
