﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace ATMLinq
{
	public static class TestDataBuilder
	{
		private static readonly Random rnd = new Random();
		private static readonly DateTime startRegDate = new DateTime(2000, 1, 1);

		public static List<User> Users { get; set; }

		public static List<Account> Accounts { get; set; }

		public static List<TransactionHistory> TransactionHistory { get; set; }

		static TestDataBuilder()
		{
			Users = GenerateUsers(100);
			Accounts = GenerateAccounts(Users);
			TransactionHistory = GenerateTransactionHistory(Accounts);
		}

		// Для списка пользователей создать аккаунты
		private static List<Account> GenerateAccounts(List<User> users)
		{
			var result = new List<Account>();
			int id = 0;
			foreach(var user in users)
			{
				// Для каждого пользователя создать от 1 до 20 счетов
				for (int j = 0; j < rnd.Next(1, 21); j++)
				{
					result.Add(new Account
					{
						Id = id++,
						Amount = rnd.Next(0, 10000000),
						IdOwner = user.Id,
						StartDate = new DateTime().Generate(user.RegDate, (DateTime.Now - startRegDate).Days)
					});
				}
			}
			return result;
		}

		// Создание пользователей
		private static List<User> GenerateUsers(int count)
		{
			var result = new List<User>();
			for (int i = 0; i < count; i++)
			{
				result.Add(new User
				{

					Id = i,
					FirstName = $"FirstName{i}",
					LastName = $"LastName{i}",
					MiddleName = $"MiddleName{i}",
					Login = $"Login{i}",
					Password = $"Pass{i}",
					PassportData = $"Passport{i}",
					PhoneNumber = $"({i}){i}-{i}-{i}",
					RegDate = new DateTime().Generate(startRegDate, DateTime.Now)
				});
			}
			return result;
		}

		// История операций
		private static List<TransactionHistory> GenerateTransactionHistory(List<Account> accounts)
		{
			var result = new List<TransactionHistory>();
			int id = 0;
			foreach(var account in accounts)
			{
				// от 0 до 10 операций по счету
				for (int j = 0; j < rnd.Next(0, 10); j++)
				{
					result.Add(new TransactionHistory()
					{
						Id = id++,
						Date = new DateTime().Generate(account.StartDate, DateTime.Now),
						IdAccount = account.Id,
						TransactionType = (Type)rnd.Next(1, 3)
					});
				}
			}
			
			return result;
		}
	}
}
