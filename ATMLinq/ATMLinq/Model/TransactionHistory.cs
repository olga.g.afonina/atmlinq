﻿using System;

namespace ATMLinq
{
	public class TransactionHistory
	{
		public int Id { get; set; }

		public DateTime Date { get; set; }

		public Type TransactionType { get; set; }

		public int IdAccount{get;set;}
	}
}
