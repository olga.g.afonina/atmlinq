﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ATMLinq
{
	public static class DateTimeExtentions
	{
		private static readonly Random rnd = new Random();
		private static readonly int AttempsCount = 1000;

		public static DateTime Generate(this DateTime date, DateTime startDate, DateTime endDate)
		{
			if (endDate <= startDate)
				throw new ArgumentException("Конечная дата не может быть меньше начальной");

			var days = (endDate - startDate).Days;
			return startDate.AddDays(rnd.Next(days));
		}

		public static DateTime Generate(this DateTime date, DateTime startDate, int amount)
		{
			if (amount <= 0)
				throw new ArgumentOutOfRangeException("Amount");

			return new DateTime().UntilValidDate(x => startDate.AddDays(rnd.Next(amount)));
		}

		public static DateTime UntilValidDate(this DateTime date, Func<DateTime, DateTime> func)
		{
			for (int i = 0; i < AttempsCount; i++)
			{
				var result = func(date);
				if (result.Validate())
					return result;
			}
			throw new TimeoutException("Попытки закончились");
		}

		public static bool Validate(this DateTime date)
		{
			return date <= DateTime.Now;
		}
	}
}
