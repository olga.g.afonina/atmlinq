﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ATMLinq
{
	class Program
	{
		private static List<User> users = new List<User>();
		private static List<Account> accounts = new List<Account>();
		private static List<TransactionHistory> history = new List<TransactionHistory>();
		private static readonly string dashString = new string('-', 120);

		static void Main(string[] args)
		{
			// LINQ запросы:
			// 1.Вывод информации о заданном аккаунте по логину и паролю
			// 2.Вывод данных о всех счетах заданного пользователя
			// 3.Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту
			// 4.Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта
			// 5.Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой)

			Console.WriteLine("\t\t\t\t\t\tLINQ запросы:");
			Console.WriteLine(dashString);

			// Сгенерировать данные
			try
			{
				BuildTestData();
			}
			catch (Exception ex)
			{
				Console.WriteLine($"Что-то пошло не так: {ex.Message}");
				Console.WriteLine("Перезапустите приложение");
				return;
			}

			FindUser();
			FindAccountInfoByUser();
			FindAccountInfoDetailsByUser();
			FindInTransactionWithOwnerDetails();
			FindUsersByAmount();
		}

		static void BuildTestData()
		{
			users = TestDataBuilder.Users;
			accounts = TestDataBuilder.Accounts;
			history = TestDataBuilder.TransactionHistory;
			Console.WriteLine($"Данные сгенерированы: пользователей - {users.Count} чел., счетов - {accounts.Count} шт., история операций - {history.Count} записей");
			Console.WriteLine(dashString);
		}

		static string GetPassword()
		{
			return GetStringFromKeyboard("Пароль: ");
		}

		static string GetLogin()
		{
			return GetStringFromKeyboard("Логин: ");
		}

		static string GetStringFromKeyboard(string name)
		{
			string result;
			do
			{
				Console.Write(name);
				result = Console.ReadLine();
			}
			while (string.IsNullOrEmpty(result.Trim()));
			return result;
		}

		static int GetIntFromKeyboard(string name)
		{
			string input;
			int result;

			do
			{
				input = GetStringFromKeyboard(name);
			}
			while (!int.TryParse(input, out result));

			return result;
		}

		static void PrintCollection<T>(IEnumerable<T> query, string msg)
		{
			if (query.Any())
			{
				query
					.Select(x =>
					{
						Console.WriteLine(x.ToString());
						return x;
					})
					.ToArray();
			}
			else
				Console.WriteLine(msg);
		}

		static void FindUser()
		{
			Console.WriteLine("1. Вывод информации о заданном аккаунте по логину и паролю");
			Console.WriteLine();

			string login = GetLogin();
			string password = GetPassword();

			Console.WriteLine();

			var user = users
				.Where(x => x.Password == password && x.Login == login);

			if (user.Any())
				Console.WriteLine(user.FirstOrDefault().ToString());
			else
				Console.WriteLine("Пользователь не найден");

			Console.WriteLine();
		}

		static void FindAccountInfoByUser()
		{
			Console.WriteLine("2.1. Вывод данных о всех счетах заданного пользователя по id");

			int id = GetIntFromKeyboard("Id: ");

			var query = accounts
				.Where(x => x.IdOwner == id)
				.Join(users, // второй набор
					p => p.IdOwner, // свойство-селектор объекта из первого набора
					t => t.Id, // свойство-селектор объекта из второго набора
					(p, t) => p); // результат

			Console.WriteLine();
			PrintCollection(query, "Счета не найдены");
			Console.WriteLine();
			Console.WriteLine("2.2. Вывод данных о всех счетах заданного пользователя по логину и паролю");

			var login = GetLogin();
			var password = GetPassword();

			var query1 = users
				.Where(x => x.Login == login && x.Password == password)
				.Join(accounts, // второй набор
					user => user.Id, // свойство-селектор объекта из первого набора
					account => account.IdOwner, // свойство-селектор объекта из второго набора
					(user, account) => account); // результат

			Console.WriteLine();
			PrintCollection(query1, "Счета не найдены");
		}

		static void FindAccountInfoDetailsByUser()
		{
			Console.WriteLine();
			Console.WriteLine("3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту");
			int id = GetIntFromKeyboard("Id: ");
			Console.WriteLine();

			var query = history
				.Join(accounts,
				history => history.IdAccount,
				account => account.Id,
				(history, account) => new
				{
					UserId = account.IdOwner,
					AccountId = history.IdAccount,
					AccountAmount = account.Amount,
					AccountStartDate = account.StartDate,
					TransactionId = history.Id,
					TransactionDate = history.Date,
					history.TransactionType
				})
				.Where(x => x.UserId == id)
				.OrderBy(x => x.AccountStartDate)
				.ThenBy(x => x.TransactionDate)
				.GroupBy(x => x.AccountId)
				.Select(
					group => new
					{
						AccountId = group.Key,
						group.Select(x => x).FirstOrDefault().AccountAmount,
						group.Select(x => x).FirstOrDefault().AccountStartDate,
						History = group.Select(x => new
						{
							x.TransactionId,
							x.TransactionDate,
							x.TransactionType
						})
					});

			foreach (var q in query)
			{
				Console.WriteLine(dashString);
				Console.WriteLine($"Id счета: {q.AccountId} Остаток по счету: {q.AccountAmount.ToString("C0")} Дата регистрации: {q.AccountStartDate.ToShortDateString()}");
				Console.WriteLine(dashString);

				foreach (var h in q.History)
				{
					Console.WriteLine($"{h.TransactionId} {h.TransactionDate.ToShortDateString()} {h.TransactionType}");
				}
			}
		}

		static void FindInTransactionWithOwnerDetails()
		{
			Console.WriteLine();
			Console.WriteLine("4. Вывод данных о всех операциях пополенения счёта с указанием владельца каждого счёта");
			Console.ReadKey();
			var query =
			   history
			   .Where(x => x.TransactionType == Type.In)
			   .Join(accounts,
					history => history.IdAccount,
					account => account.Id,
					(history, account) =>
						new
						{
							UserId = account.IdOwner,
							AccountId = history.IdAccount,
							TransactionId = history.Id,
							TransactionDate = history.Date,
							history.TransactionType,
						})
			  .Join(users,
					transaction => transaction.UserId,
					user => user.Id,
					(transaction, user) =>
						new
						{
							transaction.UserId,
							user.FirstName,
							user.LastName,
							user.MiddleName,
							transaction.AccountId,
							transaction.TransactionId,
							transaction.TransactionDate,
							transaction.TransactionType,
						})
			  .GroupBy(x => x.UserId)
			  .Select(group => new
			  {
				  UserId = group.Key,
				  UserFullName = $"{group.Select(x => x).FirstOrDefault().LastName} {group.Select(x => x).FirstOrDefault().FirstName} {group.Select(x => x).FirstOrDefault().MiddleName}",
				  Details = group.Select(x => new
				  {
					  x.AccountId,
					  x.TransactionId,
					  x.TransactionDate,
					  x.TransactionType
				  })
				  .OrderBy(x => x.AccountId)
				  .ThenBy(x => x.TransactionDate)
			  });

			Console.WriteLine();
			// Выведем первые 20 записей
			foreach (var q in query.Take(20))
			{
				Console.WriteLine(dashString);
				Console.WriteLine($"{q.UserId} {q.UserFullName}");
				Console.WriteLine(dashString);
				foreach (var details in q.Details)
				{
					Console.WriteLine($"AccountId={details.AccountId} TransactionId={details.TransactionId} Type={details.TransactionType} Date={details.TransactionDate.ToShortDateString()}");
				}
			}
		}

		static void FindUsersByAmount()
		{
			Console.WriteLine();
			Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N");
			int n;
			do
			{
				n = GetIntFromKeyboard("N: ");

				var query = accounts
				.Where(x => x.Amount > n)
				.Join(users,
					account => account.IdOwner,
					user => user.Id,
					(account, user) => new
					{
						account.Amount,
						AccountId = account.Id,
						UserId = user.Id,
						user.FirstName,
						user.LastName,
						user.MiddleName
					})
				.GroupBy(x => new
				{
					x.UserId,
					x.FirstName,
					x.LastName,
					x.MiddleName
				})
				.Select(group => new
				{
					User = group.Key,
					Account = group.Select(x => new
					{
						x.AccountId,
						x.Amount
					})
					.OrderByDescending(x => x.Amount)
				});

				Console.WriteLine();

				foreach (var q in query)
				{
					Console.WriteLine(dashString);
					Console.WriteLine($"{q.User.UserId} {q.User.LastName} {q.User.FirstName} {q.User.MiddleName}");
					Console.WriteLine(dashString);
					foreach (var a in q.Account)
					{
						Console.WriteLine($"AccountId = {a.AccountId} Amount = {a.Amount.ToString("C0")}");
					}
				}

				if (!query.Any())
					Console.WriteLine("Не найдено");

				Console.WriteLine();
			}
			while (n != -1);
		}
	}
}
